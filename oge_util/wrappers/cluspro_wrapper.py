#!/usr/bin/env python
import click
import subprocess


def get_timeout():
    """Get the timeout from qstat -j $JOB_ID

    In the XML, it is in something like
    <JB_hard_resource_list>
      <qstat_l_requests>
        <CE_name>h_rt</CE_name>
        <CE_valtype>3</CE_valtype>
        <CE_stringval>129600</CE_stringval>

    or in the plain text, 
hard resource_list:         no_gpu=TRUE,h_rt=129600
    """
    pass


@click.command()
@click.option("--callback-url")
@click.argument("command", nargs=-1, type=click.UNPROCESSED)
def main(callback_url, command):
    timeout = get_timeout()
    status = None

    try:
        subprocess.run(command, timeout=timeout)
    except TimeoutExpired:
        # report a timeout
        status = "timeout"
        pass
    except:
        status = "failed"
        # also collect other information about the failure if possible

    status = "success"
    
    if callback_url is not None:
        # post some data to the callback_url
        pass


if __name__ == "__main__":
    main()
