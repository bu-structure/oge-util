from bs4 import BeautifulSoup
import click
import subprocess
from collections import defaultdict, Counter
from itertools import islice


@click.command(name='blame')
@click.option("--limit", default=10)
def blame(limit):
    qstat = subprocess.Popen(['qstat', '-xml'], stdout=subprocess.PIPE)
    jobs = BeautifulSoup(qstat.stdout, 'xml').find_all("job_list")
    stats = defaultdict(Counter)
    for job in jobs:
        state = job.state.string
        owner = job.JB_owner.string

        stats[owner][state] += 1

    sorted_stats = sorted(stats.items(), key=lambda x: sum(x[1].values()),
                          reverse=True)
    for owner, jobs in islice(sorted_stats, limit):
        click.echo("{}: {}".format(owner, sum(jobs.values())))


if __name__ == "__main__":
    blame()
