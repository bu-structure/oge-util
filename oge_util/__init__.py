import click

from .submit import submit
from .blame import blame


@click.group()
def cli():
    pass


cli.add_command(submit)
cli.add_command(blame)
