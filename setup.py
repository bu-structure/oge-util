from pathlib import Path
from setuptools import setup, find_packages

HERE = Path(__file__).parent.resolve()

REQUIREMENTS = (
    'click >= 5.1'
)

with open(HERE/"README.rst") as f:
    LONG_DESCRIPTION = f.read()

setup(
    name="oge_util",
    packages=find_packages(exclude=["tests"]),
    description="Convenience wrappers for Open Grid Engine",
    long_description=LONG_DESCRIPTION,
    author="Bing Xia",
    author_email="sixpi@bu.edu",
    license="MIT",
    install_requires=REQUIREMENTS,

    use_scm_version={'write_to': 'oge_util/version.py'},
    setup_requires=['setuptools_scm'],

    entry_points={
        'console_scripts': [
            'oge = oge_util:cli'
        ]
    },

    classifier=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: MIT License',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
    ],
    keywords='oge grid'
)
