========
OGE Util
========

Collection of scripts to make your life easier on the SCC.

Supported Python versions: 3.4+

Installation
============

Install oge-util using pip::

    pip install oge-util

Usage
=====

Run the :code:`oge` command to get a list of subcommands
